<?php
session_start();
require('connection.php');

if (isset($_POST['submit'])) {
	unset($_POST['submit']);
	header('Location: table.php');
exit;
} 

if (isset($_POST['new_tovar'])) {
	unset($_POST['new_tovar']);
	header('Location: new_tovar.php');
exit;
}

if (isset($_POST['add_new'])) {	
	
	$sql2 = "INSERT INTO tovar (id, name, price, quantity, id_alian) VALUES ( ?, ?, ?, ?, ?)";
	$sth = $db->prepare($sql2);
	$sth->execute([NULL, $_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['owner']]);
	unset($_POST['add_new']);
	header('Location: table.php');
exit;
} 

if (isset($_POST['up_new'])) {	
	
    $num = $_SESSION[num];
	$sql2 = "UPDATE tovar  SET  name =?, price =?, quantity =?, id_alian =?  WHERE tovar.id =? ";
	$sth = $db->prepare($sql2);
	$sth->execute([ $_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['owner'], $num]);

	unset($_POST['up_new']);
	header('Location: table.php');
exit;
}
?>