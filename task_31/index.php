<?php 
session_start();
require('config/twelve.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 31</h1>
 	<div class="condition">
 		<p>Используя наработки из задачи 15:
В имеющуюся таблицу где мы выводим список товаров — добавить колонку «Владелец» и вывести туда имя и фамилию пользователя, которому принадлежит товар. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form">
     <input type='submit' name="submit" value='Вывести список товаров'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>