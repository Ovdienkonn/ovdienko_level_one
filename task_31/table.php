<?php 
session_start();
require('config/twelve.php');
require('config/create.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 31</h1>
 	<div class="condition">
 		<p>Используя наработки из задачи 15:
В имеющуюся таблицу где мы выводим список товаров — добавить колонку «Владелец» и вывести туда имя и фамилию пользователя, которому принадлежит товар. </p>
 	</div>
 </header>
  <div class="result">
	  <h2>Таблица "Товаров"
	</h2>

	<table border="1" align="center" cellpadding="7"  width="100%"> 
		<th>№</th>
    	<th>Название</th>
    	<th>Цена</th>
    	<th>Количествол</th>
    	<th>Время создается запись</th>
    	<th>Время обновление запись</th>
    	<th>Владелец</th>
    	<th>Редактировать</th>
    	<th>Удалить</th>
<?php

$tovar = $_SESSION[tovar];


for ($i=0; $i < count($tovar); $i++) { 
	$fio =' ';
	echo "<tr>";
	  for ($j=0; $j <count($tovar[$i]); $j++) { 
		if ($j != 4 and $j != 7 and $j != 9 and $j != 8) {
		echo "<td>";
		echo $tovar[$i][$j];
		echo "</td>";
	  }
		if ($j == 8 or $j == 9) {
		$fio =$fio.' '.$tovar[$i][$j];
		}

		if ($j == 9) {
		echo "<td>";
		echo $fio;
		echo "</td>";
		}
	}
	$del=$tovar[$i][0];
		echo "<td>";
		?>
	
		<a href="config/updat.php?del=<?php echo $del;?>">Редактировать</a>
		<?php
		echo "</td>";
		echo "<td>";
	?>	
		<a href="config/delet.php?del=<?php echo $del;?>">Удалить</a>
	<?php	

	
		echo "</td>";
	echo "</tr>";
}
	
?>
	  </table>

	</div>


<div class="box-form">
 <form method='post' class="form">
     <input type='submit' name="new_tovar" value='Добавить новый товар'>

  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>