<?php 
require_once('six.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 6 </h1>
 	<div class="condition">
 		<p> Написать функцию, которая принимает два числа. И определяет, какое из этих чисел больше и возвращает разницу между этими числами в процентном соотношении. Выполнить задачу и закомитить ее в свой репозиторий, согласно требований рабочего процесса. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form">
     <input type="number" name="num1" size="20" maxlength="3" min="-100" max="100" placeholder="Введите число">
     <input type="number" name="num2" size="20" maxlength="3" min="-100" max="100" placeholder="Введите число">
     <input type='submit' name="Submit" value='Отправить'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	<div class="result">
	  <h2>Результат: <?php six($_POST['num1'], $_POST['num2']); ?> </h2>
	</div>




</body>
</html>