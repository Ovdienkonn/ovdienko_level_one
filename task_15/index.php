<?php 
session_start();
require('config/twelve.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 15</h1>
 	<div class="condition">
 		<p> Создать таблицу в базе данных с полями created_at и updated_at, по умолчанию данные поля null, когда создается запись created должно автоматически назначаться и при обновлении updated тоже устанавливать текущее время. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form">
     <input type='submit' name="submit" value='Вывести список товаров'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>