<?php 
require_once('nine2.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 9 </h1>
 	<div class="condition">
 		<p> Создать файл и записать в него какой-то текст, потом прочитать этот файл на другой странице и удалить его. </p>
 	</div>
 </header>

 <div class="box-form">
  <form method='post' class="form">
     <input type='submit' name="submit2" value='Читаем содержимое файл'>
  </form>
</div> 
	<div class="result">
			<p>Содержимое файла: <span><?php echo $file; ?> </span> </p>
	</div>
	</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>
</body>
</html>