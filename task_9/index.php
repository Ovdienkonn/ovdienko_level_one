<?php 
require_once('nine.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 9 </h1>
 	<div class="condition">
 		<p> Создать файл и записать в него какой-то текст, потом прочитать этот файл на другой странице и удалить его. </p>
 	</div>
 </header>

 <div class="box-form">
  <form method='post' class="form">
  	<textarea name="text1" cols="40" rows="5" placeholder="Введите текст для записи в файл"></textarea>
     <input type='submit' name="submit" value='Создать файл и записать текст'>
  </form>
</div> 
	

<div class="back">
	<a href="../index.php">На главную</a>
</div>
</body>
</html>