<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 13</h1>
 	<div class="condition">
 		<p> Сделать кастомный чекбокс и радио баттон. </p>
 	</div>
 </header>

 <div class="result">
	  <h2>Мой кастомный чекбокс и радио баттон.</h2>
</div>
<div class="box-form">
 <form method='post' class="form">
 	<label class="check option">
     	<input class="check_input" type='checkbox' >
     	<span class="check_box"> </span>
     	Первый
 	</label>
 	<label class="check option">
     	<input class="check_input" type='checkbox' checked>
     	<span class="check_box"> </span>
     	Второй
 	</label>
 	<label class="check option">
     	<input class="check_input" type='checkbox' disabled>
     	<span class="check_box"> </span>
     	Третий
 	</label>
 	<label class="check option">
     	<input class="check_input" type='checkbox' checked disabled>
     	<span class="check_box"> </span>
     	Четвертый
 	</label>
  </form>
</div>
<div class="box-form">
 <form method='post' class="form2">

 	<label class="rad option">
     	<input class="radio_input" type='radio' checked name="rad" >
     	<span class="radio_box"> </span>
     	Первый
 	</label>

 	<label class="rad option">
     	<input class="radio_input" type='radio' name="rad" >
     	<span class="radio_box"> </span>
     	Второй
 	</label>

 	<label class="rad option">
     	<input class="radio_input" type='radio' name="rad2" disabled>
     	<span class="radio_box"> </span>
     	Третий
 	</label>

 	<label class="rad option">
     	<input class="radio_input" type='radio' name="rad2" checked disabled>
     	<span class="radio_box"> </span>
     	Четвертый
 	</label>

  </form>
</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>