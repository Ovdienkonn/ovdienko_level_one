<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 26</h1>
 	<div class="condition">
 		<p> 
Создать два текстовых поля в который будет вводиться координаты места по нажатию на кнопки должен устанавливаться маркер на карте.

 		 </p>
 	</div>
 </header>


<div class="result">
	<div id="map"></div>

 <div class="box-form">
 <form class="form" name="formCordinat" >
     <input type="text" name="str1" placeholder="Введите строку" >
     <input type="text" name="str2" placeholder="Введите строку" >

    <input onclick="showMarkers();" type="button" value="Show All Markers">
  </form>
</div> 

</div>



 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	
  

 <script type="text/javascript">

 function initMap(t) {
    var haightAshbury = {lat: 45, lng: 45};
    map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: haightAshbury,
          mapTypeId: 'terrain'
        });
		addMarker(t);
      }

function showMarkers() {
	var str1 = Number(formCordinat.str1.value); 
	var str2 = Number(formCordinat.str2.value); 
	var mas = [str1, str2];

  var t = {lat: mas[0], lng: mas[1]};

initMap(t);
 }

function addMarker(location) {
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
      }

    </script>
<script async defer 
  src="https://maps.google.com/maps/api/js?key=AIzaSyDhv3vdgJIOpayXayN4ztRfmSq4-df54o0&callback=initMap"
  type="text/javascript"></script>

</body>
</html>