<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 32</h1>
 	<div class="condition">
Выполнить следующие задачи:</br>
- Создать простое api. Которое будет отдавать данные всех пользователей (см пункт 3) так же иметь возможность удалить данные через post запрос.</br>
	- Создать отдельный сайт. В котором данные должны получаться через curl с api, которое было создано ранее.</br>
 	</div>
 </header>

<div class="left">
	<a href="curl.php">Данные полученые с CURL</a>
</div>
<div class="left">
	<a href="api.php">Результат API</a>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

 


	
<script src="ajax.js"></script>
</body>
</html>