window.onload = function() {
var inp_name = document.querySelector('input[name=name]');
var inp_surname = document.querySelector('input[name=surname]'); 

	document.querySelector('#send').onclick = function(){
		var params = 'name='+ inp_name.value +'&'+'surname='+ inp_surname.value;
		ajaxPost(params);
	}
}

function ajaxPost (params) {
	var request = new XMLHttpRequest();

	request.onreadystatechange = function () {
		if(request.readyState == 4 && request.status == 200) {
			document.querySelector('#rest').innerHTML = request.responseText;
		}
	}

	request.open('POST','add.php');
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send(params); 
}