<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 32</h1>
 	<div class="condition">
	Выполнить следующие задачи:</br>
	- Создать форму и обработчик для нее. Если пользователь ввел неправильные данные, должна вернуться ошибка валидации и соответствующий статус. Данные должны отправляться через AJAX и сохраняться в базу данных.</br>
	- Создать простое api. Которое будет отдавать данные всех пользователей (см пункт 3) так же иметь возможность удалить данные через post запрос.</br>
	- Создать отдельный сайт. В котором данные должны получаться через curl с api, которое было создано ранее.</br>

 	</div>
 </header>

<form class="form32">
 	<input type="text" name="name" >
 	<input type="text" name="surname" >
 	<input type="button"  value='Ок' id ="send">
 	<div id="rest"></div> 
</form>

<div class="left">
	<a href="index2.php">Задание с CURL</a>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

 


	
<script src="ajax.js"></script>
</body>
</html>