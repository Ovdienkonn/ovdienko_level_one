<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 10 </h1>
 	<div class="condition">
 		<p> Создать выпадающий список с 10 странами. При выборе страны, отображаем какое сейчас там время. Допускается перезагрузка страницы. Но лучше без перезагрузки. </p>
 	</div>
 </header>

 <div class="box-form">
 <form method='post' class="form">
     <select class="sel">
       <option disabled selected>Выберите страну</option>
       <option value="0">Украина</option>
       <option value="1">Россия</option>
       <option value="2">Германия</option>
       <option value="3">Белорусcия</option>
       <option value="4">Польша</option>
       <option value="5">Япония</option>
       <option value="6">Канада</option>
       <option value="7">США</option>
       <option value="8">Венгрия</option>
       <option value="9">Аргентина</option>
     </select>
  </form>
</div> 
	<div class="result">

	  <h2>Время в выбраной стране: <span id="res"></span> </h2>

	</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>
<script type="text/javascript">
	var select = document.querySelector('select');
    var zone = [0, 1, -1, 1, -1, 7, -7, -7, -1, -5];
	select.addEventListener('change', function () {
		
		var sel = select.value;
		var timestapm = Date.now();
		for (var i = 0; i < zone.length; i++) {
			if ( i == sel ) {
				var correction = zone[i];
				var t = 3600000 * correction;
				timestapm = timestapm + t;
			}
		}
		var data = new Date(timestapm);
		var time = data.getHours()+':'+data.getMinutes();
		document.getElementById('res').innerHTML = time;
	});
</script>


</body>
</html>