<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 24</h1>
 	<div class="condition">
 		<p> <br>
Добавить аккордеон на страницу с контентом. При открытии одного блока, другой закрывается. Скрипт должен быть сделан с учетом, что количество блоков в аккордеоне может быть динамическим. Т.е. должно одинаково работать, для любого количества блоков.

 		 </p>
 	</div>
 </header>



<div class="result">
	<div id="accordion">
		<?php
			for ($i=0; $i < 5; $i++) { 
		?>
		<div class = "accordion-title ">
	 		<span> Стих <?php echo $i+1; ?> </span>
		</div>
		<div class="accordion-cont">
			<p>Как переменчиво порою наша жизнь,<br>
			Покой, уют и твой очаг,<br>
			Уже не радует твою больную душу,<br>
			Не умиляет - тебя также все те вещи,<br>
			Жизнь без которых, не мог представить ты.<br>
			<br>
			Печаль и горечь нынче короли,<br>
			Неразрушимым видеться - Тандем,<br>
			Сменяя дни и ночи в этой кенетели,<br>
			Ты проживаешь те остатки дней,<br>
			Впустую тратья не на счастья.<br>
			<br>
			Любовь и разум вечные враги,<br>
			Кому отдашь ты в этой битве предпочтение ?<br></p>
		</div>

		<?php 
		}
		?>

	
	</div>
</div>



 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	
<script src="main.js"></script>
</body>
</html>