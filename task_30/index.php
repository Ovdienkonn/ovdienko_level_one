<?php
session_start(); 
include('function.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 30</h1>
 	<div class="condition">
 Создать таблицу с категориями. <br>
Категории могут быть трех уровней: <br>
<br>
* Главная<br>
* Cаб категории <br>
* Cаб категории второго уровня<br>
<br>
Все категории должны находиться в одной таблицы и все должны быть связаны через поле parent_id. <br>
Создать минимум 50 произвольных категории, разной вложенности, главных и следующие категории должно быть минимум по 10. <br>
Вывести на странице сайта все категории с необходимой вложенностью.<br>
Сделать фильтр.<br>
 	</div>
 </header>

<div class="result30">
<div class="navigation">
<?php
$mas = ArraySort();
vivod($mas);
?>


<div class="box-form2">
	<h3>Фильтры товаров</h3>
 		<form method='post' class="form1">
 			<label>Выберите поставщика:</label>
				<?php
				$alias = MassAlians();
				for ($i=0; $i < count($alias) ; $i++) { 
	 				echo '<label><input name="check'.$alias[$i]['0'].'" type="checkbox" value='.$alias[$i]['0'].'>'.$alias[$i]['1'] .'</label>';
				}
				?>
			</br>
 			<label><input name="sort" type="radio" value="ORDER BY name ASC" checked>За алфавитном</label>
			<label><input name="sort" type="radio" value="ORDER BY name DESC">Против алфавита </label>
			</br>
			<label>Количество товара:
			<input type="range" min="0" max="10000" step="10" value="5000" id="r1" oninput="fun1()" > 
			<input type="text" id="i1" class="num-target" name="colvo" value="5000"></label>
			<input type="submit" name="vivod_tovara" value="Вывести">
 		</form>
</div>
</div>
</div>
<script type="text/javascript">
	
	function fun1() {
     var rng=document.getElementById('r1'); //rng - это ползунок
   var i1=document.getElementById('i1'); // i1 - input
    i1.value=rng.value;
}
</script>

 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>