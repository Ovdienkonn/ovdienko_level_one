<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 25</h1>
 	<div class="condition">
 		<p> 
Подключить google maps и создать на ней маркер, на любой произвольный объект.

 		 </p>
 	</div>
 </header>



<div class="result">
	<div id="map"></div>
	
</div>

<script type="text/javascript">
	
	function initMap() {
		var element = document.getElementById('map');
		var options = {
			zoom: 13,
			center: {lat: 47.82289, lng: 35.19031}
		};
		var myMap = new google.maps.Map(element, options);

		addMarker({lat: 47.82289, lng: 35.19031});

		//var marker = new google.maps.Marker({
		//	position: {lat: 47.82289, lng: 35.19031},
		//	map: myMap
		//});

		//var InfoWindow = new google.maps.InfoWindow({ 
		//	content: '<h1>Это здесь !'
		//});

		//InfoWindow.open(myMap, addMarker);

		//marker.process.addListener('click', function() {
		//	InfoWindow.open(myMap, marker);
		//});

		function addMarker(cordinates) {
			var marker = new google.maps.Marker({
			position: cordinates,
			map: myMap
		});
		}

	}

</script>

 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	
  <script async defer 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhv3vdgJIOpayXayN4ztRfmSq4-df54o0&callback=initMap"
  type="text/javascript"></script>
</body>
</html>