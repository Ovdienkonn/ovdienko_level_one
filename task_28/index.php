<?php 
include('sum.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 28</h1>
 	<div class="condition">
 		<p> 
Написать функцию сложения массивов.<br>
Необходимо сделать форму с двумя textarea. В них заполняются данный в виде матрицы<br>
1 2 3 4<br>
4 5 3 2<br>
3 4 5 2<br>
<br>
и<br>
<br>
3 4 3 2<br>
2 3 2 1 1<br>

Размеры матриц могут быть разными. <br>

Вывести на экран результат сложения этих матриц по след. условиям:<br>

* Если ячейка присутствует в обеих матрицах — сложить их значения.<br>

* Если ячейка присутствует только в одной матрице — просто добавить ее значения в результирующую.<br>

* Не достающие ячейки добавить как 0.<br>

Пример ответа для представленных выше параметров:<br>
4 6 6 6 0<br>
6 8 5 3 1<br>
3 4 5 2 0<br>
 		 </p>
 	</div>
 </header>

<div class="box-form">
  <form method='post' class="form" >
  <label >Первая матрица</label>
  	<textarea  name="text1" cols="40" rows="5" placeholder="Введите массивы как показано в примере выше"><?php echo $_POST['text1']; ?></textarea>
  	<label>Вторая матрица</label>
  	<textarea name="text2" cols="40" rows="5" placeholder="Введите массивы как показано в примере выше"><?php echo $_POST['text2']; ?></textarea>
     <input type='submit' name="submit" value='Сложить массивы'>
  </form>
</div> 
<label>Результат сложения:</label>
<div class="result28">
 

<textarea name="text3" cols="40" rows="5" ><?php echo $m; ?></textarea>

</div>



 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>