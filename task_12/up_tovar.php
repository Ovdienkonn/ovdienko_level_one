<?php 
session_start();
require('config/twelve.php');

?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 12</h1>
 	<div class="condition">
 		<p> Подключиться к БД.
Получить список всех товаров.
Вывести их на экран в алфавитном порядке. В виде таблицы.
Добавить сслыки на редактирование товара и его удаление.
Так же добавить возможность создания товара, с выбором владельца этого товара. </p>
 	</div>
 </header>
  <div class="result">
	

	</div>
<?php $mas_up = $_SESSION[mas_up];

?>
<div class="box-form">
 <form method='post' class="form">
 <label class="desc"> Измените название товара: </label>
 <input type="text" name="name" pattern="^[А-Яа-яЁё\s]+$" value="<?php echo  $mas_up['1']; ?>">
 <label class="desc"> Измените цену  товара: </label>
 <input type="text" name="price" pattern="\d+(\.\d{1,2}+)?" value="<?php echo  $mas_up['2']; ?>">
 <label class="desc"> Измените дколичество товара: </label>
 <input type="text" name="quantity" pattern="[0-9]+" maxlength="5" value="<?php echo  $mas_up['3']; ?>">

 <select class="owner" name="owner">
       <option value="not" disabled selected >Выберите владельца товара</option>
       <?php 
       $alian = $_SESSION[alian];
       	for ($i=0; $i <count($alian); $i++) { 
       		if ( $alian[$i][0] == $mas_up['4']){
			echo '<option value="'.$alian[$i][0].'" selected>'.$alian[$i][1].'</option>';
       		}else{
   			echo '<option value="'.$alian[$i][0].'">'.$alian[$i][1].'</option>';
   			}
        	}
    	?>
  </select>
     <input type='submit' name="up_new" value='Добавить'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>