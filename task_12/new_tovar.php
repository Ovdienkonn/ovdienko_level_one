<?php 
session_start();
require('config/twelve.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 12</h1>
 	<div class="condition">
 		<p> Подключиться к БД.
Получить список всех товаров.
Вывести их на экран в алфавитном порядке. В виде таблицы.
Добавить сслыки на редактирование товара и его удаление.
Так же добавить возможность создания товара, с выбором владельца этого товара. </p>
 	</div>
 </header>
  <div class="result">
	

	</div>

<div class="box-form">
 <form method='post' class="form">
 <label class="desc"> Введите название товара: </label>
 <input type="text" name="name" pattern="^[А-Яа-яЁё\s]+$" placeholder="Формат ввода: Яблоко ">
 <label class="desc"> Введите цену  товара: </label>
 <input type="text" name="price" pattern="\d+(\.\d{1,2}+)?"placeholder="Формат ввода: 10.30">
 <label class="desc"> Введите дколичество товара: </label>
 <input type="text" name="quantity" pattern="[0-9]+" maxlength="5" placeholder="Формат ввода: 1000">

 <select class="owner" name="owner">
       <option value="not" disabled selected >Выберите владельца товара</option>
       <?php 
       $alian = $_SESSION[alian];
       	for ($i=0; $i <count($alian); $i++) { 
   			echo '<option value="'.$alian[$i][0].'">'.$alian[$i][1].'</option>';
        	}
    	?>
  </select>
     <input type='submit' name="add_new" value='Добавить'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>