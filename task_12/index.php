<?php 
session_start();
require('config/twelve.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 12</h1>
 	<div class="condition">
 		<p> Подключиться к БД.
Получить список всех товаров.
Вывести их на экран в алфавитном порядке. В виде таблицы.
Добавить сслыки на редактирование товара и его удаление.
Так же добавить возможность создания товара, с выбором владельца этого товара. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form">
     <input type='submit' name="submit" value='Вывести список товаров'>
  </form>
</div>

<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>