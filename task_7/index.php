
<?php 
require_once('seven.php');
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 7 </h1>
 	<div class="condition">
 		<p> В некоторых базах данных, запрещается использовать смешанные типа данных.<br>
 			Пример:

 			<br><br>SELECT ‘test’ IN (‘test2’, ‘test33’, ‘test’) - запрос выполнится
			<br><br>SELECT ‘test’ IN (11, 2, ‘test’, 2) - запрос не выполнится
			<br><br>SELECT 25 IN (11, 2, 25) - запрос выполнится
 		<br><br>Создать функцию которая будет экранировать значения при необходимости, в зависимости от других значений. </p>
 	</div>
 </header>

 <div class="box-form">
 <form method='post' class="form">
     <input type="text" name="str" placeholder="Введите строку">
     
     <input type='submit' name="Submit" value='Отправить'>
  </form>
</div> 
	<div class="result">

	  <h2>Результат: <?php seven ($_POST['str']); ?> </h2>

	</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>
</body>
</html>