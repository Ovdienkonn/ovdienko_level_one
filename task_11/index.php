<?php 
require_once('eleven.php');
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 11</h1>
 	<div class="condition">
 		<p> Создать форму в которую пользователь будет вводить два числа, выбирать из выпадающего списка математическую операцию («+» «-» «*» «/») и на экране должен получать результат. На стороне php дописать валидацию на вводимые значения. Должны быть только числа. В противном случае выводить сообщение об ошибке. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form">
     <input type="text" name="num1" placeholder="Введите число">
<label class="desc"> Выберите операцию:
     <select name="operator" class="sel-to">
       <option disabled selected></option>
       <option value="+">+</option>
       <option value="-">-</option>
       <option value="*">*</option>
       <option value="/">/</option>
     </select></label>
     <input type="text" name="num2" placeholder="Введите число">
     <input type='submit' name="Submit" value='Подсчитать'>
  </form>
</div>
<div class="result">
	  <h2>Результат: 
	  <?php
	  $num1 = htmlspecialchars($_POST['num1']);
	  $num2 = htmlspecialchars($_POST['num2']); 
	  eleven ($num1, $num2, $_POST['operator']); 
	  ?> 
	</h2>
	</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	




</body>
</html>