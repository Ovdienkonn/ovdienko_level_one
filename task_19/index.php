<?php 
session_start();
require_once('nineteen.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 19</h1>
 	<div class="condition">
 		<p> Форму из задачи номер 9: необходимо данные сохранять в базу данных, предварительно защитив их от sql injection. <br>

 		 </p>
 	</div>
 </header>

<div class="box-form">
  <form method='post' class="form">
  	<textarea name="text-form" cols="40" rows="5" placeholder="Введите текст для записи в базу"></textarea>
     <input type='submit' name="submit" value='Записать данные в базу'>
  </form>
</div> 

<div class="result">
<h2>Таблица "Test"
	</h2>

	<table border="1" align="center" cellpadding="7"  width="100%"> 
		<th>№</th>
    	<th>Текст</th>
    	
<?php

$test = $_SESSION[test];

for ($i=0; $i < count($test); $i++) { 
	echo "<tr>";
	  for ($j=0; $j <count($test[$i]); $j++) { 
		
		echo "<td>";
		echo $test[$i][$j];
		echo "</td>";
	  }
	
	
		
	
	echo "</tr>";
}
	
?>
	  </table>
	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>