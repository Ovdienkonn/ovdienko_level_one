
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задание 8 </h1>
 	<div class="condition">
 		<p> Создать форму, с несколькими типами элементов формы.
        Вывести данные, заполненные в форме на страницу под этой формой. </p>
 	</div>
 </header>

 <div class="box-form">
 <form method='post' class="form">
     <input type="text" name="str" placeholder="Текстовая строка">
     <input type="password" name="pas" placeholder="Строка преднозначеная для пароля">
     <input type="number" name="num" size="20" maxlength="3" min="-100000" max="100000" placeholder="Числовая строка">
     <input type='submit' name="Submit" value='Отправить'>
  </form>
</div> 
	<div class="result">
	<p>Вывод текстовой строки: <span><?php echo $_POST['str']; ?> </span> </p>
	<p>Вывод строки пароля: <span><?php echo $_POST['pas']; ?> </span> </p>
    <p>Вывод вывод числового поля:  <span><?php echo $_POST['num']; ?></span> </p>
	</div>
<div class="back">
	<a href="../index.php">На главную</a>
</div>
</body>
</html>