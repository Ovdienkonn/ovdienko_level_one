<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
	 <script src="function.js" dafer></script>
</head>
<body>
 <header>
 	<h1>Задание 33</h1>
 	<div class="condition">
Создать форму с неограниченной возможностью добавления инпутов.</br> 
</br> 
Пример:</br> 
ФИО</br> 
Адрес</br> 
Названия книг, что прочитали (и тут должна быть кнопка добавить еще)</br> 
Колличество добавлений не ограничено. </br> 
Так же для каждой добавленной строки — должна быть иконка удалить с работающим функционалом. </br> 
Засабмитить эту форму. </br> 
Придумать структуру БД для хранения такой информации. </br> 
Сохранить форму в БД.</br> 

 	</div>
 </header>

<form class="form33" method="POST" action="add.php">
<label>ФИО:</label>
 	<input type="text" name="fio" >
<label>Адрес:</label>
 	<input type="text" name="adress">
<label>Название книги что прочитали:</label>
	<label id="book-0"><input type="text" name="book-0">
    <input type="button" name="plus" value="+" onclick="addInput()"></label>
 	<input type="submit"  value='Ок' >
</form>



<div class="back">
	<a href="../index.php">На главную</a>
</div>

 


	

</body>
</html>