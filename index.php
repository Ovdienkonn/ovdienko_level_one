<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1> Задания <span>Flexi IT</span> </h1>
 </header>
	<div class="box">
	  <?php for ($i=6; $i <=33; $i++) { ?>
	   <div class="task">
	 	  <a href="task_<?php echo $i; ?>/index.php" class="task-num"> Задание <?php echo $i; ?></a>
	   </div>	
 	  <?php }  ?>
	</div>

</body>
</html>