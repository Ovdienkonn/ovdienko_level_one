<?php
require_once('search.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="jquery.datetimepicker.min.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
	 <script src="jquery.js"></script>
	 <script src="jquery.datetimepicker.full.js"></script>
</head>
<body>
 <header>
 	<h1>Задание 16</h1>
 	<div class="condition">
 		<p> Создать два текстовых поля и подключить datepicker.
При выборе диапазона, вывести пользователей с датой рождения в этом диапазоне. </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form" >
<label class="result">От какого времени:</label>
     <input type="text" name="data1" id="data1" value="1991-01-01">

<label class="result">По какое время: </label>  
     <input type="text" name="data2" id="data2" value="1999-01-01">

     <input type="submit" name="date-on" value="Поиск">
  </form>
</div>

<div class="result">
	<?php  if($_POST['date-on']){?>
	  <h2>Таблица "Пользователей"
	</h2>

	<table border="1" align="center" cellpadding="7"  width="100%"> 
		<th>№</th>
    	<th>Имя</th>
    	<th>фамилия</th>
    	<th>Дата рождения</th>
<?php



for ($i=0; $i < count($use); $i++) { 
	echo "<tr>";
	  for ($j=0; $j <count($use[$i]); $j++) { 
		echo "<td>";
		echo $use[$i][$j];
		echo "</td>";
	}
	echo "</tr>";
}
}	
?>
	  </table>

	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	
<script src="date.js"> </script>



</body>
</html>