
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 18</h1>
 	<div class="condition">
 		<p> 
Создать БД из трех таблиц.
Первая таблица содержит пользователей (имя, фамилия, дата рождения). 
Вторая таблица содержит список товаров, в которой сохранены id пользователей, которые являются владельцами этих товаром (название, цена, описание и т.д.). У одного пользователя может быть несколько товаров.
Третья таблица - у пользователей есть возможность «Лайкать» товары других пользователей. 
В таблице должны храниться пользователи и товары, что им понравились.
 		 </p>
 	</div>
 </header>


<div class="result">
<p> 
Перейдите на ссылку для просмотра созданой базы:<br>
<a href="https://free31.beget.com/phpMyAdmin/" target="_blank" class="link-bd">Перейти на база </a><br>
Логин:  c67885yk_ovd<br>
Пароль: kola258055<br>
Таблица №1 - "alian"<br>
Таблица №2 - "tovar"<br>
Таблица №3 - "like"<br>
 		 </p>
	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>