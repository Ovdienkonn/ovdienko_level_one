<?php 
session_start();
require_once('twenty_one.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 21</h1>
 	<div class="condition">
 		<p> Имеется массив чисел, получить из таблицы в задаче 20 список всех пользователей, чьи ID совпадают с числами из массива. <br>

 		 </p>
 	</div>
 </header>

<div class="box-form">
  <form method='post' class="form">
  	<label class="result">Введите массив чисел через запятую:</label>
  	<input type="text" name="mas_num"  value = "1,2" pattern="^[,0-9]+$">
    <input type='submit' name="submit" value='Поиск в базе'>
  </form>
</div> 

<div class="result">
	<?php

	if (!empty($_SESSION['mas_num'])) {
		
	 ?>
<h2>Таблица "Users"
	</h2>

	<table border="1" align="center" cellpadding="7"  width="100%"> 
		<th>№</th>
    	<th>Имя</th>
    	<th>Фамилия</th>
    	<th>Дата рождения</th>
    	
<?php
$mas_num = $_SESSION['mas_num'];

for ($i=0; $i < count($mas_num); $i++) { 
	echo "<tr>";
	  for ($j=0; $j < count($mas_num[$i]); $j++) { 
		echo "<td>";
		echo $mas_num[$i][$j];
		echo "</td>"; 
	}
	echo "</tr>";
}	
?>
	  </table>
<?php 
unset($_SESSION['mas_num']);
} else {

if(isset($_POST['submit'])) {
	echo "Пользователей с такими ID несуществует!";
	unset($_POST['submit']);
}

}

?>




	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>