<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 14</h1>
 	<div class="condition">
 		<p> Добавить на страницу всплывающее окно с каким-либо уведомлением.
Окно должно всплывать вместе с фоном, полностью затемняющим весь контент. Окно должно быть отцентрировано по вертикали и горизонтали. Появляется при клике на кнопку и закрываться при слике на close btn </p>
 	</div>
 </header>

<div class="box-form">
 <button class="pop-up-btn" id = "myBtn">Нажми на меня</button>
</div>



  <div class="modal myModal" id="myModal">
       <div class="modal-content">
          <button class="close"> &times;</button>
          <p>&#9830</p>
          <p>Работает!</p>
       </div>
  </div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	



<script src="pop-up.js"></script>
</body>
</html>