<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 27</h1>
 	<div class="condition">
 		<p> <br>
Создать простую страницу в которой будет два блока с текстом и кнопкой load more. 
По клику на первую отображаем скрытый текст который уже есть на странице. Сделать все средствами css.
 		 </p>
 	</div>
 </header>



<div class="result">

<input type="checkbox" id="raz" class="del" checked="checked"/>
<label for="raz" class="del">Load more</label>

<div class="text"><p>Для управления стилем соседних элементов используется символ плюса (+), который устанавливается между двумя селекторами E и F. Пробелы вокруг плюса не обязательны. Стиль при такой записи применяется к элементу F, но только в том случае, если он является соседним для элемента E и следует сразу после него. Рассмотрим несколько примеров.</p></div>



</div>



 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>