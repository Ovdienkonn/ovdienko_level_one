<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 23</h1>
 	<div class="condition">
 		<p> Работа с array + object:<br>
- Создать массив и создать объект. Посмотреть их длину .length<br>
- Создать функцию которая будет перебирать значения и выводить их в консоль.<br>
- Удалить значение с порядковым номером 3 в созданной ранее функции.<br>
- Посмотреть длину .length для массива и для объекта после удаления.<br>
- Создать массив или объект в котором ключ будет динамическим. <br>

 		 </p>
 	</div>
 </header>



<div class="result">
Смотри консоль!
</div>
<script type="text/javascript">
var fruits = ['Яблоко', 'Банан', 'Груша', 'Слива'];
console.log(fruits.length);

var promise = {
  'var'  : 'text',
  'array': [1, 2, 3, 4],
  'var2'  : 'text2',
  'var3'  : 'text3'

};
console.log(promise['var'].length);
console.log(promise['array'].length);

function selectionItem( elem) {
	var a=0;
for (var index in elem) {
   a = a+1;
 if (a == 3) {
	delete elem[index];
	}
	//console.log(index);
    console.log(elem[index]);

}
}

selectionItem(fruits);
selectionItem(promise);

var num = 'one';
var bag = {
  [num]: 5 // bag.appleComputers = 5
};
selectionItem(bag);
	</script>
 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>