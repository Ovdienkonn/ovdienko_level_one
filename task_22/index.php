<?php 
session_start();
require_once('twenty_two.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 22</h1>
 	<div class="condition">
 		<p> В таблице из задачи 20 получить список пользователей у которых уже прошел ДР в этом году и тех у кого ДР будет через 10 дней.
 <br>

 		 </p>
 	</div>
 </header>

<div class="box-form">
  <form method='post' class="form">
  	<label class="result">Введите количество дней до ДР:</label>
  	<input type="number" name="num" size="20" maxlength="3" min="1" max="100" value = "10">
    <input type='submit' name="submit" value='Вывести список пользователей'>
  </form>
</div> 

<div class="result">
	<?php

	if (!empty($_SESSION[users])) {
		
	 ?>
<h2>Таблица "Users"
	</h2>

	<table border="1" align="center" cellpadding="7"  width="100%"> 
		<th>№</th>
    	<th>Имя</th>
    	<th>Фамилия</th>
    	<th>Дата рождения</th>
    	
<?php
$users = $_SESSION[users];

for ($i=0; $i < count($users); $i++) { 
	echo "<tr>";
	  for ($j=0; $j < count($users[$i]); $j++) { 
		echo "<td>";
		echo $users[$i][$j];
		echo "</td>"; 
	}
	echo "</tr>";
}	
?>
	  </table>
<?php 
unset($_SESSION[users]);
} else {

if(isset($_POST['submit'])) {
	echo "Пользователей с ДР через такое время нет!";
	unset($_POST['submit']);
}

}

?>


	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>