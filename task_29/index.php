<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link rel="stylesheet" href="style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 29</h1>
 	<div class="condition">
Создать два сайта. <br>
Один должен всегда сохранять в базу адрес с которого пришел клиент. Данный параметр можно найти в глобальной переменной сервер. 
На втором сайте нужно создать три, четыре, любое произвольное количество страниц и ссылку на первый сайт. 
Заблокировать посещение сайта с определённые ссылки (если видим что пользователь пришел с такой страницы). 
Должен быть соответствующий статус в header и определённый контент, что доступ закрыт с данной ссылки. Если заходим с другой, контент нормальный.
 	</div>
 </header>

 

<div class="result29">

<a href="http://flexi/task_29/page_one.php">Страница №1 - переход с данной страницы на страницу сохранения</a> <br>
<a href="http://flexi/task_29/page_two.php"  target="_blank">Ссылка №2 - переход на другую страницу</a> <br>
<a href="http://flexi/task_29/page_three.php"  target="_blank">Ссылка №3 - переход на другую страницу</a> <br>

</div>



 
<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>