<?php
session_start();
include('connection.php');

$sql2 = "INSERT INTO transitions (id, name, url) VALUES ( ?, ?, ?)";
$sth = $db->prepare($sql2);
$sth->execute([NULL, $_SESSIONS['url_host'], $_SESSIONS['url_referrer']]);
unset($_SESSIONS['url_referrer']);
unset($_SESSIONS['url_host']);

?>