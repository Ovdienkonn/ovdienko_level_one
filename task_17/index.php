<?php 
//phpinfo();
include('load.php');
//include('dowland.php')
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задания Flexi</title>
	 <link rel="stylesheet" href="../style.css"/>
	 <link href="https://fonts.googleapis.com/css?family=Lobster|Russo+One&display=swap" rel="stylesheet">
</head>
<body>
 <header>
 	<h1>Задание 17</h1>
 	<div class="condition">
 		<p> Выполнить следующую последовательность:<br>
 		Создать форму, выбрать файл и отправить на сервер.<br>
 		Отправить несколько файлов на сервер и сохранить их.<br>
 		Вывести на странице файл. Если это картинка просто выводим ее, если нет, по клику скачиваем. <br>

 		 </p>
 	</div>
 </header>

<div class="box-form">
 <form method='post' class="form"  enctype="multipart/form-data">
	
   		 
   		 <input type="file" name="upload_file[]" multiple>
   		 <input type="file" name="upload_file[]" multiple>
     <input type="submit" name="btn_upload" value="Загрузить">
  </form>
</div>

<div class="result">

<?php 

$dir = 'file_upload/';
$files = scandir($dir);
for ($i=2; $i < count($files); $i++) { 
$link_files =  pathinfo($files[$i]);
echo '<a href="file_upload/'.$link_files[basename].'">'.$link_files[basename].'</a>';
echo ' ';
}



?>
	</div>


<div class="back">
	<a href="../index.php">На главную</a>
</div>

	

</body>
</html>